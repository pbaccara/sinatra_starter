# -*- mode: ruby -*-
# vi: set ft=ruby :

PROJECT_NAME      = ::File.read(::File.join(::File.dirname(::File.expand_path(__FILE__)), '.project_name').strip)
BOX               = 'debian/buster64'
MACHINE_MEMORY    = 4096
GUI               = false

RUBY_VERSION      = ::File.read(::File.join(::File.dirname(::File.expand_path(__FILE__)), '.ruby-version').strip)
RUBY_GEMSET       = ::File.read(::File.join(::File.dirname(::File.expand_path(__FILE__)), '.ruby-gemset').strip)

WITH_APACHE       = false # to provision with an Apache2 HTTP server
WITH_PASSENGER    = false # to provision a Phusion Passenger for passenger web server instead of puma

NETWORK_INTERFACE = nil # Name of the network interface to bridge
PRIVATE_IP        = '10.0.1.10'
GUEST_PORT        = WITH_APACHE == true ? 80 : 9292
HOST_PORT         = 3001

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = BOX

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL

  # Virtualbox options
  config.vm.provider "virtualbox" do |vb|
    vb.name = PROJECT_NAME
    vb.gui = GUI
    vb.memory = MACHINE_MEMORY
  end

  config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/host.pub"

  #Uncomment this to have a public network bridge
  config.vm.network :public_network, bridge: NETWORK_INTERFACE unless NETWORK_INTERFACE.nil?
  config.vm.network :private_network, ip: PRIVATE_IP, mask: "255.255.255.0"
  config.vm.network :forwarded_port, guest: GUEST_PORT, host: HOST_PORT

  # allow you to connect as root `ssh root@10.0.1.10`
  config.vm.provision "shell", path: "./vagrant/scripts/root_ssh.sh"

  config.vm.provision "shell", path: "./vagrant/scripts/requirements.sh"
  config.vm.provision "shell", path: "./vagrant/scripts/bootstrap.sh", privileged: false
  config.vm.provision "shell", path: "./vagrant/scripts/rvm.sh", args: "#{RUBY_VERSION} #{RUBY_GEMSET}", privileged: false
  config.vm.provision "shell", path: "./vagrant/scripts/capistrano.sh", privileged: false

  config.vm.provision "file", source: "./config.yml", destination: "~/shared/config.yml"
  config.vm.provision "file", source: "./.credentials", destination: "~/shared/.credentials"

  if WITH_APACHE == true
    config.vm.provision "shell", path: "./vagrant/scripts/apache_phusion_passenger.sh", args: WITH_PASSENGER.to_s
    conf_filename = "000-puma_#{PROJECT_NAME}.conf"
    conf_file = WITH_PASSENGER == false ? "apache_with_proxy.conf" : "apache_phusion_passenger.conf"
    config.vm.provision "file", source: "./vagrant/#{conf_file}", destination: "~/#{conf_filename}"
    config.vm.provision "shell", path: "./vagrant/scripts/virtual_host.sh", args: conf_filename
  end

end
