module Http
  extend self

  include Sinatra::Logger::Helpers

  LOGS = {
    general: %w[url request_method path_info host port],
    response_header: %w[content_charset content_length content_type media_type script_name],
    request_header: %w[accept_language accept_encoding authority cookies ip path referer scheme user_agent xhr?],
    query: %w[form_data? query_string],
    response: %w[body.read GET POST]
  }.freeze

  def request_log(request)
    logger.info "============== NEW Request =============="

    LOGS.each do |log_type, keys|
      logger.info "#{log_type} :"
      keys.each { |key| logger.info "  #{key} : #{key.split('.').inject(request, :send)}" }
    end
  end

end
