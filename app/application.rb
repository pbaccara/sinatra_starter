require File.join(File.dirname(__FILE__), %w[lib http])

class Application < ::Sinatra::Application

  class MissingConfigurationError < StandardError
    def initialize(missing_field)
      super "Missing '#{missing_field}', please check your config.yml"
    end
  end

  configure do
    enable :logging

    register Sinatra::Namespace
    register Sinatra::Logger
    register Sinatra::StrongParams

    # Get config.yml parameters
    config_filename = ::File.join(ROOT_PATH, 'config.yml')
    config_file = ::File.exist?(config_filename) ? YAML.load_file(config_filename) : {}

    default_config = config_file.fetch 'default', {}
    env_config = config_file.fetch Sinatra::Base.environment.to_s, {}
    config = default_config.merge env_config

    %w[].each { |required_option|
      raise MissingConfigurationError.new(required_option) unless config.include? required_option
    }

    set :config, config
  end

  configure :development do
    enable :raise_errors, :logging

    register Sinatra::Reloader
  end

  before do
    # bind Sinatra::Logger with Rack
    env['rack.logger'] = logger
    # change filename if you want to log errors in a dedicated file
    env["rack.errors"] = ErrorLogger.new settings.log_file
  end

  use Rack::Auth::Basic, 'Login' do |username, password|
    username == self.credentials['username'] && password == self.credentials['password']
  end

  def self.credentials
    @credentials ||= YAML.load_file(::File.join(ROOT_PATH, '.credentials').strip)
  end

  namespace '/v1' do

    get '*' do
      Http.request_log request

      'Get request'
    end

    post '*', allows: [:id] do
      Http.request_log request

      params.to_json
    end

    put '*', needs: [:id], allows: [:id] do
      Http.request_log request

      params.to_json
    end

    delete '*' do
      Http.request_log request

      'Delete request'
    end
  end

end
