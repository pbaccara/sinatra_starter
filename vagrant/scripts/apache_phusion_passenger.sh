#!/bin/bash

apt update
apt install -y apache2 apache2-doc
if [ "$1" == true ]; then
  apt install -y libapache2-mod-passenger
fi
apt clean
apt autoremove

if [ "$1" == true ]; then
  a2enmod passenger
else
  a2enmod proxy proxy_http
fi

service apache2 restart
