#!/bin/bash

mkdir -p /root/.ssh
chmod -R 700 /root/.ssh
cat /home/vagrant/.ssh/host.pub > /root/.ssh/authorized_keys
chmod 644 /root/.ssh/authorized_keys
