#!/bin/bash

echo "umask -S 002" >> /home/vagrant/.bashrc
mkdir -p /home/vagrant/.ssh
chmod -R 700 /home/vagrant/.ssh
cat /home/vagrant/.ssh/host.pub > /home/vagrant/.ssh/authorized_keys
chmod 644 /home/vagrant/.ssh/authorized_keys