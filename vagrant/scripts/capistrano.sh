#!/bin/bash

mkdir -p /home/vagrant/shared/log
mkdir -p /home/vagrant/shared/tmp/cache
mkdir -p /home/vagrant/shared/tmp/pids
mkdir -p /home/vagrant/shared/tmp/sockets