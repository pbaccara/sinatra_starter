#!/bin/bash

rm -f /etc/apache2/sites-enabled/000-default.conf
mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/100-default.conf
ln -s /etc/apache2/sites-available/100-default.conf /etc/apache2/sites-enabled/100-default.conf

chown root:root /home/vagrant/$1
mv /home/vagrant/$1 /etc/apache2/sites-available/$1
ln -s /etc/apache2/sites-available/$1 /etc/apache2/sites-enabled/$1
service apache2 restart
