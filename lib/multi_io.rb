class MultiIO

  def initialize(*targets)
     @targets = targets
  end

  def <<(*args)
    @targets.each { |t| t.send(:<<, *args) }
  end

  def puts(*args)
    @targets.each { |t| t.puts(*args) }
  end

  def write(*args)
    @targets.each { |t| t.write(*args) }
  end

  def flush
    @targets.each { |t| t.flush }
  end

  def close
    @targets.each(&:close)
  end

end
