require 'simplecov'

SimpleCov.profiles.define PROJECT_NAME do

  load_profile "test_frameworks"

  add_filter %r{^/config/}

  add_group "Application", "app/"
  add_group "Libraries", "lib/"

  track_files "{app,lib}/**/*.rb"
end
