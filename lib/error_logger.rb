class ErrorLogger

  def initialize(file)
    log_file = ::File.new(file, 'a+')
    log_file.sync = true
    @targets = Sinatra::Base.test? ? log_file : MultiIO.new(STDOUT, log_file)
  end

  def puts(msg)
    @targets.puts
    @targets.write "-- ERROR -- #{Time.now.strftime("%d %b %Y %H:%M:%S %z")}: "
    @targets.puts msg
  end

  def flush(*args)
    @targets.flush
  end

  def <<(*args)
    @targets.send(:<<, *args)
  end

end
