require 'logger'

module Sinatra
  module Logger

    module Helpers

      def logger
        @logger ||= begin
          log_file = ::File.new(settings.log_file, 'a+')
          log_file.sync = true
          @logger = ::Logger.new(
            Sinatra::Base.test? ? log_file : MultiIO.new(STDOUT, log_file)
          )
          @logger.level = ::Logger.const_get((settings.logger_level || :info).to_s.upcase)
          @logger.datetime_format = "%Y-%m-%d %H:%M:%S"
          @logger
        end
      end

      private

      def settings
        ::Application.settings
      end
    end

    def self.registered(app)
      app.helpers Sinatra::Logger::Helpers

      # set the output log level : :unknown, :fatal, :error, :warn, :info, :debug
      app.set :logger_level, case Sinatra::Base.environment
        when :development then :debug
        when :test then :error
        else :info
        end
      # set the full path to the log file
      app.set :log_file, lambda { ::File.join(ROOT_PATH, 'log', "#{Sinatra::Base.environment}.log") }
    end

  end

  register Sinatra::Logger

end
