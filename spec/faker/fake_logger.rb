class FakeLogger

  attr_accessor :info, :warn, :error

  def initialize
    @info = []
    @warn = []
    @error = []
  end

  def info(msg)
    @info << msg
  end

  def warn(msg)
    @warn << msg
  end

  def error(msg)
    @error << msg
  end

end