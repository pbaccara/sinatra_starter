RSpec.describe 'MultiIO' do

  before do
    @first_io = StringIO.new
    @second_io = StringIO.new
    @multi_io = MultiIO.new(@first_io, @second_io)
  end

  it '#<<' do
    input = 'test'
    @multi_io << input
    @first_io.rewind
    @second_io.rewind

    expect(@first_io.gets).to eq(input)
    expect(@second_io.gets).to eq(input)
  end

  it '#puts' do
    input = 'test'
    @multi_io.puts input
    @first_io.rewind
    @second_io.rewind

    expect(@first_io.gets).to eq("#{input}\n")
    expect(@second_io.gets).to eq("#{input}\n")
  end

  it '#write' do
    input = 'test'
    @multi_io.write input
    @first_io.rewind
    @second_io.rewind

    expect(@first_io.gets).to eq(input)
    expect(@second_io.gets).to eq(input)
  end

  it '#write' do
    input = 'test'
    @multi_io.flush
    @first_io.rewind
    @second_io.rewind

    expect(@first_io.gets).to be_nil
    expect(@second_io.gets).to be_nil
  end

  it '#write' do
    input = 'test'
    @multi_io.close

    expect(@first_io.closed?).to be true
    expect(@second_io.closed?).to be true
  end

end