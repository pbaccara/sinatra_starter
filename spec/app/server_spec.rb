RSpec.describe 'Application' do

  before do
    @username = 'username'
    @password = 'password'
    allow(::Application).to receive(:credentials).and_return({
      'username' => @username,
      'password' => @password
    })
  end

  context 'v1' do

    it 'responds to get' do
      basic_authorize @username, @password
      get '/v1/'

      expect(last_response).to be_ok
      expect(last_response.body).to eq('Get request')
    end

    it 'responds to post' do
      basic_authorize @username, @password
      params = { id: '1', not_allowed: '2' }
      post '/v1/', params

      expect(last_response).to be_ok
      expect(last_response.body).to eq(params.except(:not_allowed).to_json.to_s)
    end

    context 'when put' do
      it 'responds when needed params is given' do
        basic_authorize @username, @password
        params = { id: '1', not_allowed: '2' }
        put '/v1/', params

        expect(last_response).to be_ok
        expect(last_response.body).to eq(params.except(:not_allowed).to_json.to_s)
      end

      it "doesn't responds when needed params isn't given" do
        basic_authorize @username, @password
        params = { not_allowed: '2' }
        put '/v1/', params

        expect(last_response.status).to eq(400)
      end
    end

    it 'responds to delete' do
      basic_authorize @username, @password
      delete '/v1/'

      expect(last_response).to be_ok
      expect(last_response.body).to eq('Delete request')
    end

  end

end
