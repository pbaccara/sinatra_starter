# Sinatra Starter

A Simple starter based on **[Sinatra](http://sinatrarb.com)** with :

* [Puma](https://github.com/puma/puma)
* [Rack](https://github.com/rack/rack)
    * [Rack-Attack](https://github.com/rack/rack-attack)
    * [Rack-Cache](https://github.com/rtomayko/rack-cache)
    * [Rack-Test](https://github.com/rack/rack-test)
* [Rspec](https://rspec.info)
* [Rubocop](https://github.com/rubocop/rubocop)
* [Simplecov](https://github.com/simplecov-ruby/simplecov)
* [Docker](https://docs.docker.com)
* [Capistrano](https://capistranorb.com)
    * [Capistrano-Puma](https://github.com/seuros/capistrano-puma)
    * [Capistrano-RVM](https://github.com/rvm/rvm1-capistrano3)
* [Vagrant](https://www.vagrantup.com/intro)

This starter provide :

* 3 environments :
    * development
    * test
    * production
* 3 types of logs (*./log*) :
    * environment log (*./lib/sinatra/logger.rb*)
    * rack errors log (*./lib/error_logger.rb*)
    * Rack-attack log (*./config/initializers/rack_attack_logger.rb*)

# Local install

Make a global search on `sinatra_starter` and replace to your desired application name.

```bash
gem install bundler
bundle install
rackup
```

* Install Docker : https://docs.docker.com/engine/install/
* Install Vagrant : https://www.vagrantup.com/docs/installation

# Specs

Execute Rspec to handle tests :

```bash
rspec spec/
```

To generate a test coverage report :

```bash
COVERAGE=ON rspec spec/
```

# Docker

## Development

```bash
cd ./docker/development
PROJECT_NAME=sinatra-starter docker-compose build
PROJECT_NAME=sinatra-starter docker-compose up
docker-compose down
```

# Vagrant

Vagrant is usefull when you want to test your capistrano rules or if you need a complex environment that need multiple services in one instance.

```bash
vagrant up # start your Vagrant VM
vagrant provision # provision Vagrant VM with some requirements and bootstraps
cap vagrant puma:systemd:config puma:systemd:enable # create puma as a systemd service
cap vagrant deploy # deploy the application to the Vagrant VM
```

Log to your VM via ssh protocol (if you have previously built a VM of your project and connected to it be sure to remove previous connection entries from your `~/.ssh/known_host`, usually ssh connection on vagrant start with a `|1|`)

```bash
vagrant ssh # as vagrant user
ssh root@10.0.1.10 # as root
```

Stop your VM

```bash
vagrant halt # stop your Vagrant VM
```

# logrotate

## On size

Use [`Ruby::Logger`](https://ruby-doc.org/stdlib-3.0.0/libdoc/logger/rdoc/Logger.html) shift_age and shift_size to configure the rotation.

## On frequency

Use Unix logrotate instead of `Ruby::Logger`.
Add to `/etc/logrotate.conf` this chunk of code :
```bash
/home/deploy/APPNAME/current/log/*.log {
  daily
  missingok
  rotate 7
  compress
  delaycompress
  notifempty
  copytruncate
}
```
* **daily** : Rotate the log files each day. You can also use weekly or monthly here instead.
* **missingok** : If the log file doesn’t exist, ignore it
* **rotate 7** : Only keep 7 days of logs around
* **compress** : GZip the log file on rotation
* **delaycompress** : Rotate the file one day, then compress it the next day so we can be sure that it won’t interfere with the Rails server
* **notifempty** : Don’t rotate the file if the logs are empty
* **copytruncate** : Copy the log file and then empties it. This makes sure that the log file Rails is writing to always exists so you won’t get problems because the file does not actually change. If you don’t use this, you would need to restart your Rails application each time.

# TODO

* build bin scripts to automate deploy
* add Jenkins
* add Sentry