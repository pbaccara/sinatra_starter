ROOT_PATH = ::File.dirname(::File.expand_path('..', __FILE__))
PROJECT_NAME = ::File.read(::File.join(ROOT_PATH, '.project_name').strip)

require 'rubygems'
require 'bundler/setup'

Bundler.require :default
Bundler.require Sinatra::Base.environment

require 'sinatra/strong-params'
require 'yaml'

require File.join(ROOT_PATH, %w[lib sinatra logger])
require File.join(ROOT_PATH, %w[lib error_logger])
require File.join(ROOT_PATH, %w[lib multi_io])

require File.join(ROOT_PATH, ['config', 'environments', Sinatra::Base.environment.to_s])

require File.join(ROOT_PATH, %w[app application])
