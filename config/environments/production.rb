require 'active_support/notifications' # Keep it before rack-attack requirements

=begin
require 'rack/cache'

CACHE_PATH = ::File.join(ROOT_PATH, %w[tmp cache])

use Rack::Cache,
  metastore:    "file:#{::File.join(CACHE_PATH, 'meta')}",
  entitystore:  "file:#{::File.join(CACHE_PATH, 'body')}",
  verbose:      true
=end

require "rack/attack"

# Rack-Attack initializers
require ::File.join(ROOT_PATH, %w[config initializers rack_attack])
# Rack-Attack logger initializers
require ::File.join(ROOT_PATH, %w[config initializers rack_attack_logger])

use Rack::Attack
