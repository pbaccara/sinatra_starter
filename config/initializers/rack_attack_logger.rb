log_file = ::File.new(::File.join(ROOT_PATH, 'log', "rack-attack.log"), 'a+')
log_file.sync = true
LOGGER = Logger.new(log_file)
LOGGER.info('Cache init')

ActiveSupport::Notifications.subscribe(/rack_attack/) do |name, start, finish, request_id, payload|
  req = payload[:request]
  msg = [req.env['rack.attack.match_type'], req.ip, req.request_method, req.fullpath, ('"' + req.user_agent.to_s + '"')].join(' ')

  %i[throttle blocklist].include?(req.env['rack.attack.match_type']) ? LOGGER.warn(msg) : LOGGER.info(msg)
end
