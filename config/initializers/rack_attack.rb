require 'active_support/cache'

class Rack::Attack

  # cache.store = ::ActiveSupport::Cache::FileStore.new(::File.join(ROOT_PATH, %w[tmp cache rack_attack]))
  cache.store = ::ActiveSupport::Cache::MemoryStore.new

  self.blocklisted_response = lambda do |request|
    # Using 503 because it may make attacker think that they have successfully
    # DOSed the site. Rack::Attack returns 403 for blocklists by default
    [ 503, {}, ['Blocked']]
  end

  # Use Allow2ban mechanic instead of association of fail2Ban to avoid double check Rack::Auth::Basic
  blocklist('fail2ban basic auth protection') do |req|
    Rack::Attack::Fail2Ban.filter("basic-auth-ban:#{req.ip}", maxretry: 3, findtime: 1.minute, bantime: 1.hour) do
      auth = Rack::Auth::Basic::Request.new(req.env)
      auth.credentials != self.credentials
    end
  end

  private

  def self.credentials
    @credentials ||= begin
      data = YAML.load_file(::File.join(ROOT_PATH, '.credentials').strip)
      [data['username'], data['password']]
    end
  end

end
